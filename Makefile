clean: clean_pyc
clean_pyc:
	find . -type f -name "*.pyc" -delete || true

migrate:
	DJANGOENV=$(ENVIRONMENT) saturn/manage.py migrate --noinput

reusedb_unittests:
	DJANGO_SETTINGS_MODULE=saturn.testing.settings REUSE_DB=1 DJANGOENV=testing $(PYTHON) saturn/manage.py test $(TESTONLY) --with-spec --spec-color --nologcapture --keepdb

unittests:
	DJANGO_SETTINGS_MODULE=saturn.testing.settings DJANGOENV=testing saturn/manage.py test $(TESTONLY) --with-spec --spec-color --nologcapture

run:
	$(PYTHON) saturn/manage.py runserver_plus 0.0.0.0:8000 --settings=saturn.$(ENVIRONMENT).settings || python manage.py runserver 0.0.0.0:8000 --settings=saturn.$(ENVIRONMENT).settings

shell:
	$(PYTHON) saturn/manage.py shell_plus --settings=saturn.$(ENVIRONMENT).settings || python manage.py shell --settings=saturn.$(ENVIRONMENT).settings

show_urls:
    $(PYTHON) saturn/manage.py show_urls --settings=saturn.$(ENVIRONMENT).settings || python manage.py shell --settings=saturn.$(ENVIRONMENT).settings

validate_templates:
    $(PYTHON) saturn/manage.py validate_templates --settings=saturn.$(ENVIRONMENT).settings || python manage.py shell --settings=saturn.$(ENVIRONMENT).settings
