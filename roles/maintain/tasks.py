import os
import sys

from jinja2 import Environment, FileSystemLoader
from invoke import run, task


def _template_file_from_environment(template, destination, template_dir='/var/www/deploy/'):
    environment = Environment(loader=FileSystemLoader(template_dir))
    template = environment.get_template(template)
    rendered_template = template.render(os.environ)
    if os.path.isfile(destination):
        os.unlink(destination)
    with open(destination, 'w') as f:
        f.write(rendered_template)


@task
def beat():
    run('test -e /tmp/celerybeat.pid && rm /tmp/celerybeat.pid || true')
    command = 'cd /tmp && DJANGOENV=%(ENVIRONMENT)s '\
              'PYTHONUNBUFFERED=1 '\
              '/var/www/saturn/manage.py '\
              'celery beat '\
              '--uid www-data '\
              '--gid www-data '\
              '--loglevel DEBUG ' % os.environ
    run(command)


@task
def cron():
    if 'CONCURRENCY' not in os.environ.keys():
        os.environ['CONCURRENCY'] = '4'

    command = 'cd /tmp && DJANGOENV=%(ENVIRONMENT)s '\
              'PYTHONUNBUFFERED=1 '\
              '/var/www/saturn/manage.py '\
              'celery worker '\
              '--loglevel DEBUG '\
              '--uid www-data '\
              '--gid www-data '\
              '-Q cron '\
              '-c %(CONCURRENCY)s '\
              '--hostname "cron-%(ENVIRONMENT)s@%%h" ' % os.environ
    run(command)


@task
def nginx():
    _template_file_from_environment('nginx.conf', '/etc/nginx/sites-enabled/default')
    run('nginx -g "daemon off;"')


@task
def api():
    _template_file_from_environment('nginx.api.conf', '/etc/nginx/sites-enabled/default')
    _template_file_from_environment('supervisor.healthcheck.conf', '/etc/supervisor/conf.d/healthcheck.conf')
    _template_file_from_environment('app.wsgi', '/var/www/app.wsgi')
    _template_file_from_environment('uwsgi.ini', '/var/www/uwsgi.ini')
    # Run supervisord so that the nginx healthcheck instance is running
    run('supervisord')
    run('uwsgi --ini /var/www/uwsgi.ini')


@task
def webserver():
    _template_file_from_environment('nginx.healthcheck.conf', '/etc/nginx/sites-enabled/default')
    _template_file_from_environment('supervisor.healthcheck.conf', '/etc/supervisor/conf.d/healthcheck.conf')
    _template_file_from_environment('app.wsgi', '/var/www/app.wsgi')
    _template_file_from_environment('uwsgi.ini', '/var/www/uwsgi.ini')
    # Run supervisord so that the nginx healthcheck instance is running
    run('supervisord')


@task
def worker():
    try:
        env = {
            'QUEUE': os.environ['QUEUE'],
            'ENVIRONMENT': os.environ.get('ENVIRONMENT')
        }
    except Exception as e:
        print e
        sys.exit(1)

    run('mkdir -p /mnt/in-context')
    run('chown www-data:www-data /mnt/in-context')

    command = 'PYTHONUNBUFFERED=1 DJANGOENV=%(ENVIRONMENT)s '\
              'saturn/manage.py '\
              'celery worker '\
              '-Q %(QUEUE)s '\
              '--loglevel=DEBUG '\
              '--uid www-data '\
              '--gid www-data '\
              '--hostname "%(ENVIRONMENT)s@%%h"' % env
    run(command)


@task
def pgbouncer():
    _template_file_from_environment('pgbouncer.ini', '/tmp/pgbouncer.ini')
    command = 'tools/pgbouncer -u www-data /tmp/pgbouncer.ini'
    run("apt-get update")
    run("apt-get install -y libc-ares2 libc-ares-dev")
    run("chmod +x /var/www/tools/pgbouncer")
    run(command)


@task
def development():
    run('pip install -U pip')
    run('pip install -r requirements/development.txt')
    # _template_file_from_environment('nginx.conf', '/etc/nginx/sites-enabled/default')
    # _template_file_from_environment('supervisor.development.conf', '/etc/supervisor/conf.d/development.conf')
    # run('make -C /var/www dev')
    run('supervisord -n')