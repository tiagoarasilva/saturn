import os
import sys

from jinja2 import Environment, FileSystemLoader
from invoke import run, task


def _template_file_from_environment(template, destination, template_dir='/var/www/deploy/'):
    environment = Environment(loader=FileSystemLoader(template_dir))
    template = environment.get_template(template)
    rendered_template = template.render(os.environ)
    if os.path.isfile(destination):
        os.unlink(destination)
    with open(destination, 'w') as f:
        f.write(rendered_template)

@task
def webserver():
    _template_file_from_environment('nginx.healthcheck.conf', '/etc/nginx/sites-enabled/default')
    _template_file_from_environment('supervisor.healthcheck.conf', '/etc/supervisor/conf.d/healthcheck.conf')
    _template_file_from_environment('app.wsgi', '/var/www/app.wsgi')
    _template_file_from_environment('uwsgi.ini', '/var/www/uwsgi.ini')
    # Run supervisord so that the nginx healthcheck instance is running
    run('supervisord')

@task
def development():
    run('pip install -U pip')
    run('pip install -r requirements/development.txt')
    # _template_file_from_environment('app.wsgi', '/var/www/tools/app.wsgi')
    # run('gunicorn saturn.wsgi:application -w 2 -b :8000')
    # _template_file_from_environment('nginx.conf', '/etc/nginx/sites-enabled/default')
    # _template_file_from_environment('supervisor.development.conf', '/etc/supervisor/conf.d/development.conf')
    # run('make -C /var/www dev')
    run('supervisord -n')
