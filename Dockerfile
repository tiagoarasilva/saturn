############################################################
# Dockerfile to run a Django-based web application
# Based on an Ubuntu Image
############################################################
FROM ubuntu:14.04
MAINTAINER tiago.arasilva@goutlook.com

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_GB.UTF-8
ENV LC_ALL en_GB.UTF-8

#
# Install Apt Packages
#
RUN apt-get update                                                                                           && \
    apt-get upgrade -y                                                                                       && \
    apt-get install curl -y                                                                                  && \
    apt-get clean && apt-get autoclean                                                                       && \
    find /var/lib/apt/lists/ -type f -delete                                                                 && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

RUN apt-get update                                              && \
    apt-get install -y bind9-host                                  \
                       curl                                        \
                       geoip-bin                                   \
                       gettext                                     \
                       git-core                                    \
                       gawk                                        \
                       imagemagick                                 \
                       iputils-ping                                \
                       language-pack-en                            \
                       less                                        \
                       libcurl4-openssl-dev                        \
                       libffi-dev                                  \
                       libgeos-c1                                  \
                       libmagickwand-dev                           \
                       libmemcached-tools                          \
                       libxml2-dev                                 \
                       libxslt-dev                                 \
                       memcached                                   \
                       net-tools                                   \
                       nginx-extras                                \
                       perl                                        \
                       pgbouncer                                   \
                       postgresql-client-9.4                       \
                       postgresql-server-dev-9.4                   \
                       python-imaging                              \
                       python-chardet-whl                          \
                       python-colorama-whl                         \
                       python-distlib-whl                          \
                       python-html5lib-whl                         \
                       python-pip-whl                              \
                       python-requests-whl                         \
                       python-setuptools-whl                       \
                       python-six-whl                              \
                       python-urllib3-whl                          \
                       python-pip                                  \
                       python-dev                                  \
                       rsyslog                                     \
                       socat                                       \
                       software-properties-common                  \
                       sudo                                        \
                       supervisor                                  \
                       gunicorn                                    \
                       telnet                                      \
                       unattended-upgrades                         \
                       unzip                                       \
                       vim                                         \
		       mongodb					   \
                       libmysqlclient-dev                          \
                       mysql-server                                \
                       wget                                     && \
    apt-get clean && apt-get autoclean                          && \
    find /var/lib/apt/lists/ -type f -delete

#
# Deadsnakes Python 2.7 point-releases
#
RUN add-apt-repository -y ppa:fkrull/deadsnakes-python2.7       && \
    apt-get update && apt-get install -y python2.7              && \
    apt-get clean && apt-get autoclean                          && \
    find /var/lib/apt/lists/ -type f -delete

#
# Install Pip Requirements
#
ADD requirements /var/www/requirements
RUN pip install -r /var/www/requirements/common.txt

# Patch Nginx Config to Disable Security Tokens
RUN sed -i -e 's/# server_tokens off;/server_tokens off;/g' /etc/nginx/nginx.conf
