var _static = function (asset) {
  return "{% static '' %}" + asset;
};

$(window).on('on', function () {
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year
    format: 'dd/mm/yyyy',
    close: 'choose'
  });
});
