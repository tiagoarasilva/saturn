from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Address(models.Model):

    HOME = u'home'
    BUSINESS = u'business'
    OTHER = u'other'

    ADDRESS_TYPE = ((HOME, _('Home')),
                    (BUSINESS,_('business')),
                    (OTHER, _('other')),)

    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    company = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255)
    extended_address = models.CharField(max_length=255)
    locality = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=16)
    country = models.CharField(max_length=2)
    phone = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    address_type = models.CharField(max_length=255, choices=ADDRESS_TYPE, default=HOME)
    address_book = models.ForeignKey('addresses.AddressBook', null=True, blank=False, related_name='addresses')

    def __unicode__(self):
        return u'%s - %s, %s - %s' % (self.name, self.street_address, self.postal_code, self.country)

    @property
    def name(self):
        first_name = self.first_name or u""
        last_name = self.last_name or u""
        return u'{first_name} {last_name}'.format(last_name=last_name, first_name=first_name).strip()

    @property
    def country_full(self):
        return self.country


class AddressBookGroup(models.Model):
    address_book = models.ForeignKey('addresses.AddressBook', related_name='address_book_groups')
    profile = models.ForeignKey('profiles.Profile', related_name="groups")


class AddressBook(models.Model):
    profile = models.ForeignKey('profiles.Profile', related_name="address_books")
    is_default = models.BooleanField(default=False, null=False)
    is_business = models.BooleanField(default=False, null=False)
    is_home = models.BooleanField(default=False, null=False)
    is_other = models.BooleanField(default=False, null=False)

    def __unicode__(self):
        return self.profile.display_name

    def _ensure_saved(self):
        if not self.id:
            self.save(force_insert=True)

    @property
    def default(self):
        return self.is_default

    @property
    def business(self):
        return self.is_business

    @property
    def home(self):
        return self.is_home

    @property
    def other(self):
        return self.is_other

    @property
    def address_list(self):
        return self.addresses.all()
