from django.test import TestCase

import addresses.tests.factories
import profiles.tests.factories
import addresses.models


class AddressesTest(TestCase):

    def setUp(self):
        self.profile = profiles.tests.factories.ProfileFactory()
        self.addressbook = addresses.tests.factories.AddressBookFactory(profile=self.profile)

    def test_add_address_successfully_to_address_book(self):
        address = addresses.tests.factories.AddressFactory(
            first_name='address 1', address_book=self.addressbook)
        address2 = addresses.tests.factories.AddressFactory(
            first_name='address 2', address_book=self.addressbook)
        address3 = addresses.tests.factories.AddressFactory(
            first_name='address 2', address_book=self.addressbook)

        self.assertEqual(3, self.addressbook.addresses.count())

    def test_can_create_addressbook_and_associate_to_user(self):
        addressbook = addresses.tests.factories.AddressBookFactory(profile=self.profile)

        address = addresses.tests.factories.AddressFactory(
            first_name='address 1', address_book=addressbook)

        addresses_list = addressbook.address_list

        self.assertIn(address, addresses_list)

    def test_returns_list_of_addresses(self):
        addresses.tests.factories.AddressFactory(
            first_name='address 1', address_book=self.addressbook)
        addresses.tests.factories.AddressFactory(
            first_name='address 2', address_book=self.addressbook)
        addresses.tests.factories.AddressFactory(
            first_name='address 2', address_book=self.addressbook)

        list_addresses = self.addressbook.address_list

        self.assertEqual(3, list_addresses.count())