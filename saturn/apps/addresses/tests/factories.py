import factory
import factory.django
import addresses.models

from profiles.tests.factories import ProfileFactory, UserFactory


class AddressBookFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = addresses.models.AddressBook
    is_home = True
    profile = factory.SubFactory(ProfileFactory)


class AddressFactory(factory.django.DjangoModelFactory):

    FACTORY_FOR = addresses.models.Address
    address_book = factory.SubFactory(AddressBookFactory)
    first_name = 'Linezap'
    last_name = 'Lisbon'
    company = 'LxCOM'
