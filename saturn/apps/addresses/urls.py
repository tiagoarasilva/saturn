from django.conf.urls import url

import curriculums.views

url_adresses_patterns = [

    # URLS FOR CURRICULUMS
    url(r'^$', curriculums.views.ResumeView.as_view(), name='home-cv'), # STILL TO DECIDE
    url(r'^resume/(?P<slug>[a-zA-Z0-9-_]+)/$', curriculums.views.JobsView.as_view(), name='resume'),
    url(r'^jobs/(?P<slug>[a-zA-Z0-9-_]+)/$', curriculums.views.JobsView.as_view(), name='jobs'),
    url(r'^education/(?P<slug>[a-zA-Z0-9-_]+)/$', curriculums.views.ResumeView.as_view(), name='jobs'),

]