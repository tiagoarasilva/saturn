from django.test import TestCase

import profiles.tests.factories
import profiles.models


class UserAccountModelTest(TestCase):

    def setUp(self):
        self.user = profiles.tests.factories.UserFactory(username='stuff')
        self.user_2 = profiles.tests.factories.UserFactory(username='linezap_2')

    def test_create_user(self):
        self.assertEqual('stuff', self.user.username)

    def test_create_user_with_specific_email(self):
        self.user.email = 'tiago@test.com'
        self.user.save()

        self.assertEqual(self.user.email, 'tiago@test.com')

    def test_create_user_with_specific_password(self):
        self.user.password = '123password'
        self.user.save()

        self.assertEqual(self.user.password, '123password')

    def test_create_user_with_staff_access(self):
        self.user.is_staff = True
        self.user.save()

        self.assertEqual(self.user.is_staff, True)

    def test_can_create_more_than_one_user_at_the_same_time(self):
        self.assertNotEqual(self.user, self.user_2)


class FollowTests(TestCase):

    def setUp(self):
        self.profile = profiles.tests.factories.ProfileFactory()

    def test_can_follow_another_user(self):
        who_is_followed = profiles.tests.factories.ProfileFactory()
        follow = profiles.tests.factories.FollowFactory(who_follows=self.profile, who_is_followed=who_is_followed)

        self.assertIsNotNone(follow)
