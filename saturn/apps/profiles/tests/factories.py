import factory
import factory.django
import uuid

from django.contrib.auth import get_user_model
# from accounts.tests.factories import UserFactory

import profiles.models


class ProfileFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = profiles.models.Profile
    first_name = "Linezap"
    last_name = 'lxcom'
    slug = factory.LazyAttribute(lambda u: "-%s" % str(uuid.uuid4())[0:12])


class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = get_user_model()
    username = factory.Sequence(lambda n: "linezap-%s" % n)
    password = "linezap"
    email = factory.LazyAttribute(lambda u: "%s@linezap.example.com" % u.username)

    profile = factory.RelatedFactory(ProfileFactory, name='user')

    @classmethod
    def _prepare(cls, create, **kwargs):
        user = super(UserFactory, cls)._prepare(create, **kwargs)
        password = kwargs.pop('password', None)
        if password:
            user.set_password(password)
            if create:
                user.save()
        return user


class FollowFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = profiles.models.Follow
