from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import models
from lib.audit.models import LooselyAuditedModel


class Profile(models.Model):
    """
    A profile, linked to a user. All users MUST have a profile, whether they
    can log into the website or not, as the profile contains information about
    attribution etc.
    """

    class VerificatonStatus:
        VERIFIED = 'verified'
        UNVERIFIED = 'unverified'
        MANUALLY_VERIFIED = 'manually-verified'

        CHOICES = (
            (VERIFIED, 'Verified'),
            (UNVERIFIED, 'Unverified'),
            (MANUALLY_VERIFIED, 'Manually verified')
        )

    class UserType:
        RECRUITER = 'recruiter'
        SIMPLEUSER = 'simpleuser'

        CHOICES = (
            (RECRUITER, 'A recruiter'),
            (SIMPLEUSER, 'A single user'),
        )

    class NewsLetterEmailFrequency:
        DAILY = 'daily'
        WEEKLY = 'weekly'
        MONTHLY = 'monthly'

        CHOICES = (
            (DAILY, 'Daily'),
            (WEEKLY, 'Weekly'),
            (MONTHLY, 'Monthly')
        )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, related_name='profile')
    slug = models.SlugField(max_length=255, unique=True, default=None, blank=True)
    display_name = models.CharField(max_length=255, blank=True, default="A linezap user")
    is_public = models.BooleanField(_("Profile publicly visible"), default=True, blank=True,
                                    help_text=_("Other users can see your public profile"))

    user_type = models.CharField(max_length=50, choices=UserType.CHOICES, default=UserType.SIMPLEUSER,
                                 null=True, blank=True)

    verified = models.CharField(max_length=50, choices=VerificatonStatus.CHOICES,
                                default=VerificatonStatus.UNVERIFIED, null=True, blank=True)

    newsletter_frequency = models.CharField(max_length=50, choices=NewsLetterEmailFrequency.CHOICES,
                                            default=NewsLetterEmailFrequency.MONTHLY,
                                            null=True, blank=True)
    language = models.CharField(_("Language"), max_length=5, default=settings.LANGUAGE_CODE,
                                choices=settings.VISIBLE_LANGUAGES)
    profile_name = models.TextField(blank=True, max_length=45)
    first_name = models.TextField(blank=True, max_length=255)
    middle_name = models.TextField(blank=True, max_length=255)
    last_name = models.TextField(blank=True, max_length=45)
    birth_date = models.DateTimeField(blank=True, null=True)
    phone_number = models.TextField(blank=True, max_length=45)
    mobile_number = models.TextField(blank=True, max_length=45)
    fiscal_number = models.TextField(blank=True, max_length=45)
    website = models.CharField(null=True, blank=True, max_length=255)
    industry_position = models.CharField(null=True, blank=True, max_length=255)
    incomplete_signup = models.BooleanField(default=False, blank=True)
    signup_ip = models.GenericIPAddressField(blank=True, null=True)
    signup_referrer = models.CharField(blank=True, null=True, max_length=2048)
    date_left = models.DateTimeField(blank=True, null=True)
    date_first_seen = models.DateField(blank=True, null=True)
    date_joined_secondary = models.DateField(blank=True, null=True)
    number_of_followers = models.IntegerField(null=True, blank=True)

    objects = models.Manager()

    def __unicode__(self):
        return self.first_name

    def get_birthday_date_formatted(self):
        if not self.birth_date:
            return datetime.datetime.now()
        return datetime.datetime.strftime(self.birth_date, "%Y-%m-%d")

    @property
    def email(self):
        return self.user.email

    @property
    def total_being_followed(self):
        return self.number_of_followers

    @property
    def job_application(self):
        return self.job_applications.all()

    @property
    def address_book(self):
        return self.address_books.last()


class Follow(models.Model):
    who_follows = models.ForeignKey(Profile, related_name="follows")
    who_is_followed = models.ForeignKey(Profile, related_name="who_is_followed")
    follow_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return str(self.follow_time)
