# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-15 04:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_auto_20170522_1209'),
    ]

    operations = [
        migrations.CreateModel(
            name='Follow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('follow_time', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='follow',
            name='who_follows',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='follows', to='profiles.Profile'),
        ),
        migrations.AddField(
            model_name='follow',
            name='who_is_followed',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='who_is_followed', to='profiles.Profile'),
        ),
    ]
