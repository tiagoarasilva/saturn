# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-21 19:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('fiscal_number', models.TextField(blank=True, max_length=45)),
                ('industry', models.CharField(blank=True, max_length=100)),
                ('number_of_employee', models.IntegerField(blank=True)),
                ('website', models.CharField(blank=True, max_length=255, null=True)),
                ('description', models.TextField(max_length=1000)),
                ('ceo', models.CharField(blank=True, max_length=255)),
                ('company_type', models.CharField(blank=True, choices=[('private', 'Private'), ('llp', 'Limited Liability Partnership'), ('llc', 'Limited Liability Company'), ('corporation', 'Corporation')], default='private', max_length=50)),
            ],
        ),
    ]
