from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _



class Company(models.Model):
    """
    Company or organization
    """

    class CompanyType:
        """
        Type of company/organization
        """
        PRIVATE = _('private')
        LLP = _('llp')
        LLC = _('llc')
        CORPORATION = _('corporation')

        CHOICES = (
            (PRIVATE, 'Private'),
            (LLP, 'Limited Liability Partnership'),
            (LLC, 'Limited Liability Company'),
            (CORPORATION, 'Corporation')
        )

    name = models.CharField(max_length=255)
    fiscal_number = models.TextField(blank=True, max_length=45)
    industry = models.CharField(max_length=100, blank=True)
    number_of_employee = models.IntegerField(blank=True)
    website = models.CharField(null=True, blank=True, max_length=255)
    description = models.TextField(max_length=1000)
    ceo = models.CharField(max_length=255, blank=True)
    # TODO: The company type will not be a choice but a table with company type per country
    company_type = models.CharField(max_length=50, choices=CompanyType.CHOICES,
                                    default=CompanyType.PRIVATE, blank=True)

    def __unicode__(self):
        return self.name
