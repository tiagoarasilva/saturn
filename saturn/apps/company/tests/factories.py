import factory
import factory.django
import uuid

from company.models import Company


class CompanyFactory(factory.django.DjangoModelFactory):
    """
    Model factory for Company testing
    """
    FACTORY_FOR = Company
    name = 'Dummy Corporation'
    description = 'A dummy company'
