from django.test import TestCase

from company.tests.factories import CompanyFactory
from company.models import Company


class CompanyCreationModelTest(TestCase):

    def setUp(self):
        self.company = CompanyFactory(name='Private Comp.')

    def test_create_simple_company(self):
        self.assertEqual('Private Comp.', self.company.name)
    
    def test_create_with_company_type(self):
        self.company.company_type = Company.CompanyType.LLP
        self.company.save()

        self.assertEqual(self.company.company_type, 'llp')