import django.test

import search.tests.factories


class TestSimpleUser(django.test.TestCase):

    def setUp(self):
        self.user = search.tests.factories.UserFactory()
        self.profile = search.tests.factories.ProfileFactory()

    def test_customer_email_is_the_same_as_the_user(self):
        self.assertEqual(self.user.email, self.profile.get_email())
