import factory
import factory.django
import datetime

from django.contrib.auth import get_user_model

from profiles.models import Profile


class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = get_user_model()
    username = factory.Sequence(lambda n: "lxcom-%s" % n)
    password = "lxcom"
    email = factory.LazyAttribute(lambda u: "%s@lxcom.example.com" % u.username)

    profile = factory.RelatedFactory(ProfileFactory, name="user")

    @classmethod
    def _prepare(cls, create, **kwargs):
        user = super(UserFactory, cls)._prepare(create, **kwargs)

        password = kwargs.pop('password', None)
        if password:
            user.set_password(password)
            if create:
                user.save()
        return user


class ProfileFactory(factory.django.Django):
    FACTORY_FOR = Profile
    user = factory.SubFactory(UserFactory)
    birth_date = datetime.datetime.now()
