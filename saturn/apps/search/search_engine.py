from __future__ import unicode_literals


class SearchEngine(object):
    """
    Engine responsible for returning a result set based
    on group of filters
    """


    def __init__(self):
        filters = {}