from django.forms import forms, ModelForm

import curriculums.models


class ResumeForm(ModelForm):

    class Meta:
        model = curriculums.models.Resume
        fields = ['profile']


class EducationForm(forms.Form):

    class Meta:
        model = curriculums.models.ResumeEducation
        fields = ['school', 'course', 'resume', 'start_date', 'end_date']


class JobForm(forms.Form):

    class Meta:
        model = curriculums.models.ResumeJob
        fields = ['title', 'resume', 'company', 'start_date', 'end_date']
