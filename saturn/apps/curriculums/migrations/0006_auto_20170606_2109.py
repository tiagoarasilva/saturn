# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-06 21:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('curriculums', '0005_auto_20170604_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resumeeducation',
            name='resume',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='resume_educations', to='curriculums.Resume'),
        ),
        migrations.AlterField(
            model_name='resumejob',
            name='resume',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='resume_jobs', to='curriculums.Resume'),
        ),
        migrations.AlterField(
            model_name='resumeskills',
            name='resume',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='resume_skills', to='curriculums.Resume'),
        ),
    ]
