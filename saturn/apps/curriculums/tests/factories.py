import factory
import factory.django

import curriculums.models
from profiles.tests.factories import ProfileFactory


class ResumeFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = curriculums.models.Resume
    profile = factory.SubFactory(ProfileFactory)
    about = 'This is all about a linezap profile'


class ResumeEducationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = curriculums.models.ResumeEducation
    resume = factory.SubFactory(ResumeFactory)


class ResumeJobFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = curriculums.models.ResumeJob
    resume = factory.SubFactory(ResumeFactory)
