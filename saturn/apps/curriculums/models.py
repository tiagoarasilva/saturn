from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models
from lib.audit.models import LooselyAuditedModel

import profiles.models
from job_application.models import BaseJob, BaseSkill


class Resume(LooselyAuditedModel):
    profile = models.ForeignKey(profiles.models.Profile, related_name='resume')
    slug = models.SlugField(unique=True)
    about = models.CharField(_('About'), max_length=2000, blank=True, default=None)

    def __unicode__(self):
       return self.slug

    @property
    def jobs(self):
        return self.resume_jobs.all()

    @property
    def education(self):
        return self.resume_educations.all()

    @property
    def skills(self):
        return self.resume_skills.all()


class ResumeEducation(models.Model):
    """
    A school/course entry in a resume
    """
    school = models.CharField(max_length=100, blank=True, null=True)
    course = models.CharField(max_length=100, blank=True, null=True)
    resume = models.ForeignKey(Resume, related_name='resume_educations')
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return self.resume.profile.display_name


class ResumeJob(BaseJob):
    """
    A job or employment record in a resume
    """
    resume = models.ForeignKey(Resume, related_name='resume_jobs')

    def __unicode__(self):
        return self.title


class ResumeSkills(BaseSkill):
    """
    A resume skill
    """
    resume = models.ForeignKey(Resume, related_name='resume_skills', blank=True)

    def __unicode__(self):
        return self.skill_name

