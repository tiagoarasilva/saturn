from django.views.generic import TemplateView, CreateView
from lib.common.views import UserAuthMixin

import curriculums.forms


class ResumeView(UserAuthMixin, TemplateView):
    template_name = 'curriculum/curriculum.html'

    def get_context_data(self, **kwargs):
        context = super(ResumeView, self).get_context_data(**kwargs)
        # context.update({})
        return context


class EducationView(UserAuthMixin, CreateView):
    template_name = 'curriculum/education.html'
    form_class = curriculums.forms.EducationForm

    def get_context_data(self, **kwargs):
        context = super(EducationView, self).get_context_data(**kwargs)
        # context.update({})
        return context


class JobsView(UserAuthMixin, CreateView):
    template_name = 'curriculum/jobs.html'

    def get_context_data(self, **kwargs):
        context = super(JobsView, self).get_context_data(**kwargs)
        # context.update({})
        return context
