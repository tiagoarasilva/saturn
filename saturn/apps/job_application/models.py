from __future__ import unicode_literals

from django.db import models
from profiles.models import Profile
from django.utils.translation import ugettext_lazy as _


class BaseJob(models.Model):
    """
    Abstract model for a basic job object
    """
    VAC = _("Job Vacancy")
    PER = _("PER")

    JOB_TYPE = (
        (VAC, 'Job Vacancy'),
        (PER, 'Job held by a person/Employment')
    )

    title = models.CharField(max_length=140, blank=True, null=False)
    company = models.CharField(max_length=100, blank=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    job_type = models.CharField(max_length=3, choices=JOB_TYPE, null=True)
    description = models.CharField(max_length=5000, null=True, blank=True)

    class Meta:
        abstract = True


class BaseSkill(models.Model):
    """
    Abstract model for a basic skill object
    """
    skill_name = models.CharField(max_length=140, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    weight = models.IntegerField(blank=True, null=True)

    class Meta:
        abstract = True


class JobApplication(BaseJob):
    """
    A job application with a set of requirements and contacts
    """
    reference_id = models.CharField(max_length=255, blank=True)
    slug = models.SlugField(unique=True)
    expiration_date = models.DateTimeField(blank=True, null=True)
    appl_author = models.ForeignKey(Profile, related_name='job_applications')

    def __unicode__(self):
        return self.slug

    @property
    def job_requirement(self):
        return self.job_requirments.all()


class JobRequirement(BaseSkill):
    """
    Requirement (skills or time constraints) of a job appl.
    """
    job_application = models.ForeignKey(JobApplication, related_name="job_requirements")
    must_have = models.BooleanField(default=False)
