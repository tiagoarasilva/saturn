import datetime

from django.test import TestCase


import profiles.tests.factories
import profiles.models
from job_application.tests.factories import JobApplicationFactory
from job_application.models import JobApplication


class JobApplicationCreationModelTest(TestCase):

    def setUp(self):
        #self.profile.save()
        self.job_application = JobApplicationFactory(
            title='Lowlife Programmer')

    def test_create_job_application(self):
        self.assertEqual(self.job_application.title, u'Lowlife Programmer')
    
    def test_author_job_application(self):
        self.assertEqual(
            self.job_application.appl_author.first_name, 'Linezap')
    
    def test_job_application_expiration_date(self):
        self.job_application.expiration_date = datetime.date(2017, 6, 8)
        self.job_application.save()

        self.assertEqual(self.job_application.expiration_date, datetime.date(2017, 6, 8))