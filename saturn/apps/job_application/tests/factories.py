import factory
import factory.django

import job_application.models
from profiles.tests.factories import ProfileFactory


class BaseJobFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = job_application.models.BaseJob
    title = 'Programmer'


class BaseSkillFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = job_application.models.BaseSkill
    description = 'Base skill description'


class JobApplicationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = job_application.models.JobApplication
    appl_author = factory.SubFactory(ProfileFactory)


class JobRequirementFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = job_application.models.JobRequirement
    job_application = factory.SubFactory(JobApplicationFactory)
