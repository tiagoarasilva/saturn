from __future__ import unicode_literals

import datetime

from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, FormView
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from profiles.models import Profile
from job_application.forms import JobApplicationForm
from job_application.models import JobApplication
from lib.common.views import UserAuthMixin
from job_application import utils


class JobApplFormView(UserAuthMixin, FormView):
    template_name = 'job_application/job_application.html'
    form_class = JobApplicationForm

    def get(self, request, *args, **kwargs):
        return super(JobApplFormView, self).get(request, *args, **kwargs)
    
    def get_success_url(self):
        return reverse('applications:list')
    
    def get_initial(self):
        initial = super(JobApplFormView, self).get_initial()

        #TODO: This will be replaced by the company that the recruiter works for
        initial['company'] = 'Recruitment company'

        return initial
    
    def form_valid(self, form):
        """
        Form verified submission processing
        """

        #Application author is the logged user
        try:
            author = Profile.objects.get(user=self.request.user)
        except Profile.DoesNotExist:
            msg = _("Unable to retrieve the logged user information")
            messages.add_message(self.request, messages.ERROR, msg)
            return super(JobApplFormView, self).form_valid(form)
        
        job_appl = utils.create_job_application(form.data['title'],
                                                author)

        job_appl.title = form.data['title']
        job_appl.description = form.data['description']
        job_appl.start_date = datetime.datetime.strptime(
            form.data['start_date'], "%Y-%m-%d")
        job_appl.expiration_date = datetime.datetime.strptime(
            form.data['expiration_date'], "%Y-%m-%d")

        job_appl.save()
        msg = _("The job application was created successfully")
        messages.add_message(self.request, messages.SUCCESS, msg)
        return HttpResponseRedirect(self.get_success_url())


#TODO: To be replaced by a ListView in another issue of this iteration
class ApplicationsView(UserAuthMixin, TemplateView):
    """
    View for the recruiter/company applications list
    """
    template_name = 'job_application/applications.html'

    def get_context_data(self, **kwargs):
        context = super(ApplicationsView, self).get_context_data(**kwargs)

        try:
            appl_author = Profile.objects.get(user=self.request.user)
        except Profile.DoesNotExist:
            msg = _("Unable to retrieve the logged user information")
            messages.add_message(self.request, messages.ERROR, msg)
            return super(JobApplFormView, self).form_valid(form)

        context['user_applications'] = list(
            JobApplication.objects.filter(appl_author=appl_author)
        )
        return context
