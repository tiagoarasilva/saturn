from django import forms
from django.utils.translation import ugettext_lazy as _
from job_application.models import JobApplication, JobRequirement


class JobApplicationForm(forms.Form):

    title = forms.CharField(
        label=_('Title'),
        required=True,
    )

    company = forms.CharField(
        label=_('Company'),
        required=False
    )

    start_date = forms.DateField(
        label=_('Start date'),
        required=False,
        widget=forms.DateInput(
            attrs={'class': 'datepicker', 
                   'type': 'date'} )
    )

    expiration_date = forms.DateField(
        label=_('Expiration date'),
        required=False,
        widget=forms.DateInput(
            attrs={'class': 'datepicker',
                   'type': 'date'} )
    )

    description = forms.CharField(
        label=_('Job description'),
        required=True,
        widget=forms.Textarea(
            attrs={'cols': '10',
                   'rows': '4'} )
    )

    class Meta:
        model = JobApplication
        fields = ['title', 'company', 'description']