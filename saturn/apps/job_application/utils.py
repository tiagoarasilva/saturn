from .models import JobApplication
from profiles.models import Profile
from django.template.defaultfilters import slugify
import uuid


def create_job_application(title, appl_author):
    """
    Creates a new job application instante
    """
    short_uuid = str(uuid.uuid4())
    slug = short_uuid[:8] + "-" + slugify(title)

    job_application = JobApplication.objects.create(
        slug=slug, 
        title=title, 
        job_type='VAC',
        appl_author=appl_author
    )

    return job_application
