from django.conf.urls import url
from django.views.generic import RedirectView

import job_application.views


urls_jobs_patterns = [

    # URLS FOR JOB APPLICATIONS
    # url(r'^$', job_application.views.ApplicationsView.as_view(), name='home-application'),
    url(r'^job/create/$', job_application.views.JobApplFormView.as_view(), name='create'),
    url(r'^job/list/$', job_application.views.ApplicationsView.as_view(), name='list'),
]
