from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth import login, logout
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View, TemplateView, FormView
from lib.viewslib.views import AjaxFormView

import accounts.forms
import accounts.models
import accounts.utils
import profiles.models


def login_and_handle_data_stored_in_session(user, request):
    session_key = request.session.session_key
    login(request, user)


class LoginView(FormView):
    template_name = 'login/login.html'
    form_class = accounts.forms.LoginForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(self.get_success_url())
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.get_user()
        login_and_handle_data_stored_in_session(user, self.request)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        messages.add_message(
            self.request, messages.ERROR, _("The credentials that you've entered don't match any account")
        )
        return super(LoginView, self).form_invalid(form)
    
    def get_success_url(self):
        return reverse('homepage')


class LoginAjaxView(AjaxFormView):
    template_name = 'login/login.html'
    form_class = accounts.forms.LoginForm

    def ajax_form_valid(self, form):
        user = form.get_user()
        login_and_handle_data_stored_in_session(user, self.request)
        return super(LoginAjaxView, self).ajax_form_valid(form)

    def ajax_form_invalid(self, form):
        return super(LoginAjaxView, self).form_invalid(form)


class SignUpView(FormView):
    template_name = 'login/register.html'
    form_class = accounts.forms.RegistrationForm

    def form_valid(self, form):
        first_name = form.data['first_name']
        last_name = form.data['last_name']
        email = form.data['email']
        password = form.data['password']

        username = accounts.utils.generate_username(first_name)

        try:
            user = get_user_model().objects.create_user(
                username=username, email=email, password=password,
                is_staff=False, is_superuser=False
            )

        except IntegrityError:
            msg = _("There was a problem processing your information, please try again")
            messages.add_message(self.request, messages.ERROR, _(msg[0]))
            return super(SignUpView, self).form_valid(form)
        
        user.save()
        user.get_or_create_profile(first_name=first_name, last_name=last_name)
        
        msg = _("Your profile has been created")
        messages.add_message(self.request, messages.SUCCESS, _(msg[0]))
        login_and_handle_data_stored_in_session(user, self.request)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        for errors in form.errors.items():
            key, msg = errors
            messages.add_message(self.request, messages.ERROR, _(msg[0]))
        return super(SignUpView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('homepage')


class SignUpAjaxView(AjaxFormView):
    template_name = 'login/register.html'
    form_class = accounts.forms.RegistrationForm

    def ajax_form_valid(self, form):
        first_name = form.data['first_name']
        last_name = form.data['last_name']
        email = form.data['email']
        password = form.data['password']

        username = accounts.utils.generate_username(first_name)

        try:
            user = get_user_model().objects.create_user(
                username=username, email=email, password=password,
                is_staff=False, is_superuser=False
            )

        except IntegrityError:
            return super(SignUpAjaxView, self).ajax_form_invalid(form)

        user.save()
        user.get_or_create_profile(first_name=first_name, last_name=last_name)

        login_and_handle_data_stored_in_session(user, self.request)
        return super(SignUpAjaxView, self).ajax_form_valid(form)

    def ajax_form_invalid(self, form):
        for errors in form.errors.items():
            key, msg = errors
            return super(SignUpAjaxView, self).ajax_form_invalid(form)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(self.request)
        return HttpResponseRedirect(reverse('login'))


class HomepageView(TemplateView):
    template_name = 'home/home.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(HomepageView, self).get(request, *args, **kwargs)
        return HttpResponseRedirect(reverse('login'))

    def get_object(self):
        return profiles.models.Profile.objects.get(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(HomepageView, self).get_context_data(**kwargs)
        context.update({
            'profile': self.get_object()
        })
        return context
