from __future__ import unicode_literals

import bleach

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_init, post_save
from django.dispatch import receiver
from django.contrib.auth.models import Permission as DjangoPermission
from django.utils.crypto import get_random_string
from django.conf import settings

from lib.cache.decorators import memoize_invalidate
from profiles.models import Profile


class User(AbstractUser):

    class Meta:
        db_table = 'auth_user'
        permissions = (('can_sudo_user', 'Can sudo another user'),
                       ('can_send_sms', 'Can send sms to user'),
                       ('can_invite_user', 'Can send friend request to user'),
                       ('can_recommend', 'Can recommend user'),
                       ('can_endorse_user', 'Can endorse  user'),
                       ('can_see_profile', 'Can see profile of user'))

    @memoize_invalidate
    def get_or_create_profile(self, first_name=None, last_name=None):
        try:
            return Profile.objects.get(user=self)

        except Profile.DoesNotExist:
            profile = Profile(
                user=self, slug=self.username, first_name=first_name, last_name=last_name,
                incomplete_signup=True
            )
            profile.save()

            return profile

    def to_json_dict(self, context=None):
        data = {
            'id': self.pk,
            'username': bleach.clean(self.username),
            'email_url': reverse('postman_write', args=(self.pk,))
        }

        profile = self.profile
        if profile is not None:
            data['profile'] = profile.to_json_dict(context)

        return data

    def get_primary_email(self):
        return self.email

    @staticmethod
    def post_init(sender, **kwargs):
        instance = kwargs["instance"]
        instance.first_name = bleach.clean(instance.first_name)
        instance.last_name = bleach.clean(instance.last_name)
        instance.old_email = instance.email
        instance.old_is_active = instance.is_active
        instance.old_first_name = instance.first_name
        instance.old_last_name = instance.last_name

    @staticmethod
    def post_save(sender, **kwargs):
        instance = kwargs["instance"]
        try:
            profile = instance.profile
        except Profile.DoesNotExist:
            pass
        instance.old_email = instance.email
        instance.old_first_name = instance.first_name
        instance.old_last_name = instance.last_name

post_init.connect(User.post_init, sender=User)
post_save.connect(User.post_save, sender=User)


class EmailAddressManager(models.Manager):

    def generate_key(self):
        """Generate a new random key and return it"""
        # sticking with the django defaults
        return get_random_string()

    def create_confirmed(self, email, instance=None):
        """Create an email address in the confirmed state"""
        instance = instance or getattr(self, 'instance', None)
        if not instance:
            raise ValueError('Must specify user or call from related manager')
        key = self.generate_key()
        now = timezone.now()
        # let email-already-exists exception propogate through
        instance_type = 'user' if isinstance(instance, User) else 'provider'
        address_kwargs = {
            'email': email,
            'key': key,
            'set_at': now,
            'confirmed_at': now,
            instance_type: instance
        }
        address = self.create(**address_kwargs)
        self.confirm_parallel_email(instance, email)

        return address


class AbstractEmailAddress(models.Model):
    """An email address belonging to a User"""

    class Meta:
        abstract = True

    email = models.EmailField(max_length=255)
    key = models.CharField(max_length=40, unique=True)

    set_at = models.DateTimeField(
        default=timezone.now,
        help_text='When the confirmation key expiration was set',
    )
    confirmed_at = models.DateTimeField(
        blank=True, null=True,
        help_text='First time this email was confirmed',
    )

    objects = EmailAddressManager()

    @property
    def is_confirmed(self):
        return self.confirmed_at is not None


class EmailAddress(AbstractEmailAddress):
    """An email address belonging to a User"""

    user = models.ForeignKey(User, related_name='email_addresses')

    class Meta:
        unique_together = (('user', 'email'),)
        verbose_name_plural = "email addresses"

    @property
    def is_primary(self):
        return bool(self.user.email == self.email)

    def __unicode__(self):
        return '{} <{}>'.format(self.user, self.email)


@receiver(user_logged_in)
def lang(sender, **kwargs):
    lang_code = kwargs['user'].profile.language
    kwargs['request'].session['django_language'] = lang_code


class Permission(DjangoPermission):

    class Meta:
        proxy = True
        permissions = (
            ('shorten_urls', 'Can shorten urls in the dashboard'),
        )
