import factory
import factory.django
import accounts.models

class AnonymousUser(object):
    def is_anonymous(self):
        return True

    def is_authenticated(self):
        return False


class FakeStaffUser(object):
    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True

    def is_staff(self):
        return True

    def is_superuser(self):
        return False


class FakeSuperUser(FakeStaffUser):

    def is_superuser(self):
        return True


class UserFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = accounts.models.User