from django_webtest import WebTest

from django.core.urlresolvers import reverse


class LoginAndRegisterViewTest(WebTest):

    def setUp(self):
        self.url = reverse('login')
        self.url_register = reverse('sign-up')

    def test_get_login_page(self):
        response = self.app.get(self.url)

        self.assertEqual(200, response.status_code)

    def test_register_user_profile_form_successfully(self):
        page = self.app.get(self.url_register)
        form = page.forms['register-form']
        form['first_name'] = 'Tiago'
        form['last_name'] = 'last name'
        form['email'] = 'test@test.com'
        form['password'] = '123cenas'
        form['retype_password'] = '123cenas'
        response = form.submit()

        self.assertEqual(200, response.status_code)

    def test_register_user_profile_form_with_error_for_missing_last_name(self):
        page = self.app.get(self.url_register)
        form = page.forms['register-form']
        form['first_name'] = 'Tiago'
        form['last_name'] = ''
        form['email'] = 'test@test.com'
        form['password'] = '123cenas'
        form['retype_password'] = ''
        response = form.submit()

        self.assertFormError(response, 'form', 'last_name', 'This field is required.')
        self.assertFormError(response, 'form', 'retype_password', 'This field is required.')
