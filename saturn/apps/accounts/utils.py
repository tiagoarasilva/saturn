import uuid


def generate_username(first_name):
    uuid_user = str(uuid.uuid4())
    username = first_name.lower() + "-{id}".format(id=uuid_user[:12])
    return username
