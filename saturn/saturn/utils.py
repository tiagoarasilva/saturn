# mostly resolution via AWS, separated to here to make testing easier

import os

import boto.ec2
import boto.ec2.elb
import boto.elasticache
import boto.rds2
import boto.utils

from django.core.exceptions import ImproperlyConfigured


def get_elb_endpoint(endpoint_name, region=None, default=None):
    """
    Find the EC2 ELB Address and return it
    """
    if region is None:
        region = boto.utils.get_instance_identity()['document']['region']

    elbcon = boto.ec2.elb.connect_to_region(region)
    try:
        elb = elbcon.get_all_load_balancers(endpoint_name)[0]
    except Exception as e:
        import bugsnag
        from saturn.configs.settings import BUGSNAG
        bugsnag.configure(**BUGSNAG)
        bugsnag.notify(
            e, 
            metadata={
                "Additional Info": {
                    "Requested ELB Name": endpoint_name,
                    "Target AWS Region": region,
                    "Fallback Value": default
                }
            }
        )
        if default:
            return default
        else:
            raise ImproperlyConfigured(
                'Unable to retrieve EC2::ELB Address for %s' % endpoint_name
            )

    return elb.dns_name


def get_rds_endpoint(endpoint_name, region=None, default=None):
    """
    Find the RDS Endpoint Address and return it
    """
    try:
        if region is None:
            region = boto.utils.get_instance_identity()['document']['region']

        rdscon = boto.rds2.connect_to_region(region)
        response = rdscon.describe_db_instances(endpoint_name)
        response = response['DescribeDBInstancesResponse']
        response = response['DescribeDBInstancesResult']
        return response['DBInstances'][0]['Endpoint']['Address']
    except:
        if default:
            return default
        raise ImproperlyConfigured(
            'Unable to retrieve RDS Address for %s' % endpoint_name
        )


def get_redis_elasticache_endpoint(cluster_name, region=None, default=None):
    """
    Query ElastiCache API and Get Endpoint Address of Redis Node

    In cases where there is multi-AZ there will be multiple cluster
    endpoints (e.g. cluster-001 and cluster-002). Both have identical
    node addresses and you only need to connect to one. In these cases
    just query 'cluster-001' and you should get the response you need
    """
    try:
        if region is None:
            region = boto.utils.get_instance_identity()['document']['region']

        ec = boto.elasticache.connect_to_region(region)
        response = ec.describe_cache_clusters(cache_cluster_id=cluster_name,
                                              show_cache_node_info=True)
        response = response['DescribeCacheClustersResponse']
        response = response['DescribeCacheClustersResult']['CacheClusters'][0]
        return response['CacheNodes'][0]['Endpoint']['Address']
    except:
        if default:
            return default
        raise ImproperlyConfigured(
            'Unable to retrieve ElastiCache::Redis address for %s' % cluster_name
        )


def get_memcached_elasticache_endpoint(cluster_name, region=None):
    """
    Query ElastiCache API and Get Memcache Endpoints
    """
    try:
        if region is None:
            region = boto.utils.get_instance_identity()['document']['region']

        ec = boto.elasticache.connect_to_region(region)
        response = ec.describe_cache_clusters(cache_cluster_id=cluster_name,
                                              show_cache_node_info=True)
        response = response['DescribeCacheClustersResponse']
        response = response['DescribeCacheClustersResult']['CacheClusters'][0]
        nodes = []
        for node in response['CacheNodes']:
            nodes.append(node['Endpoint']['Address'])
        return nodes
    except:
        raise ImproperlyConfigured(
            'Unable to retrieve ElastiCache::Memcached address for %s' % cluster_name
        )


def get_instance_region(region=None):
    if region:
        return region

    return boto.utils.get_instance_identity()['document']['region']


def get_instances(filters=None, region=None):
    ec2 = boto.ec2.connect_to_region(get_instance_region(region))
    instances = []
    for reservation in ec2.get_all_instances(filters=filters):
        instances.extend(reservation.instances)

    return instances


def get_instance_private_ip(instance_id='self', environment=None, region=None):
    if instance_id == 'self':
        return boto.utils.get_instance_identity()['document']['privateIp']

    filters = {
        'tag:Environment': environment,
        'instance-id': instance_id
    }
    instance = get_instances(filters, region)[0]
    return instance.private_ip_address


def get_instance_public_ip(instance_id='self', environment=None, region=None):
    if instance_id == 'self':
        instance_id = boto.utils.get_instance_identity()['document']['instance_id']

    filters = {
        'tag:Environment': environment,
        'instance-id': instance_id
    }
    instance = get_instances(filters, region)[0]
    return instance.ip_address


def find_ec2_host_by_role(rolename, environment, region=None):
    """
    :param rolename: The name of the instance role we filter by
    :type rolename: str or unicode

    :param region: The EC2 region in which to search. Defaults to host region.
    :type region: str or unicode

    :returns: All host that meet requested parameters
    :rtype: list
    """

    # If no region specified assume host region
    if region is None:
        region = boto.utils.get_instance_identity()['document']['region']

    ec2 = boto.ec2.connect_to_region(region)
    filters = {
        'tag:Environment': environment,
    }
    instances = []
    for reservation in ec2.get_all_instances(filters=filters):
        for role in reservation.instances[0].tags['Role'].split(','):
            if role == rolename:
                instances.append(reservation.instances[0])

    return instances


# Requires access to
AWS_ACCESS_KEY = "AKIAI5E2SXP2TWONDWCQ"
AWS_SECRET_KEY = "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"


def instances_by_function(instances, function):
    # filter instances that have the function tag (af-<function>=ready)
    def has_function(instance):
        if instance.tags.get('af-%s' % function, None) != 'ready':
            return False
        else:
            return True

    return [instance for instance in instances if has_function(instance)]


def instances_by_environment_and_function(environment, instances, function):
    def has_function(instance):
        if (instance.tags.get("af-%s" % function) == "ready"
                and instance.tags.get("af-environment") == environment):
            return True
        return False
    return [instance for instance in instances if has_function(instance)]


def get_aws_region():
    if os.path.exists("/etc/aws_region"):
        with open("/etc/aws_region") as fh:
            return fh.read().strip()


def make_memcached_cache(memcached_locations):
    return {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
        },
        'api': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
            'TIMEOUT': 86400 * 365,
            'KEY_PREFIX': 'api2',  # 2 => includes slugs for objects
        },
        'staticfiles': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
            'TIMEOUT': 86400 * 365,
            'KEY_PREFIX': 'staticfiles',
        },
        'depictions': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
            'TIMEOUT': 86400 * 365,
            'KEY_PREFIX': 'depictions',
        },
        'thumbnails': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
            'TIMEOUT': 86400 * 365,
            'KEY_PREFIX': 'thumbnails',
        },
        'sessions': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': memcached_locations,
        },
    }
