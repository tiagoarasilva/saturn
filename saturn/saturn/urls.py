from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.generic import RedirectView
from django.conf import settings

import accounts.views
import profiles.urls
import curriculums.urls
import addresses.urls
import job_application.urls

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # LINEZAP PORTAL
    url(r'^$', csrf_exempt(accounts.views.HomepageView.as_view()), name='homepage'),
    url(r'^login/$', accounts.views.LoginView.as_view(), name='login'),
    url(r'^sign-in/$', accounts.views.LoginAjaxView.as_view(), name='login-ajax'),

    url(r'^sign-up/$', accounts.views.SignUpView.as_view(), name='sign-up'),
    url(r'^register/$', accounts.views.SignUpAjaxView.as_view(), name='signup-ajax'),

    url(r'^logout/$', accounts.views.LogoutView.as_view(), name='logout'),

    # PROFILES
    url(r'^profile/', include(profiles.urls.url_profile_patterns, namespace='profile')),

    # CURRICULUMS
    url(r'^curriculum/', include(curriculums.urls.url_cv_patterns, namespace='cv')),

    # ADDRESSES
    url(r'^address/', include(addresses.urls.url_adresses_patterns, namespace='address')),

    # JOB APPLICATIONS AND WORKFLOWS
    url(r'^applications/', include(job_application.urls.urls_jobs_patterns, namespace='applications')),

]


