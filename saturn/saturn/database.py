# DATABASES = {
#     'default': {
#         'ENGINE': 'transaction_hooks.backends.postgresql_psycopg2',
#         'NAME': 'dev_lxcom',
#         'HOST': 'pgbouncer',
#         'USER': 'default',
#         'PASSWORD': 'bouncer',
#         'PORT': 6432
#     },
#     'tracking': {
#         'ENGINE': 'transaction_hooks.backends.postgresql_psycopg2',
#         'NAME': 'dev_tracking',
#         'HOST': 'pgbouncer',
#         'USER': 'tracking',
#         'PASSWORD': 'bouncer',
#         'PORT': 6432
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

