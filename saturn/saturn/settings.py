import os
import django
import sys

from linezap.blacklisted_domains import *
from linezap.settings import *
from saturn.database import *

DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
SITE_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
SATURN_VERSION = os.path.basename(os.path.dirname(SITE_ROOT))
DJANGOENV = None

sys.path.append(os.path.join(SITE_ROOT, 'apps'))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=#6590!xhc%(r_dh-b64nbl++@h9v^@#_*ohitn5=@nr!wss-h'

ADMINS = (
    ('Tiago Silva', 'tiago.arasilva@outlook.com'),
    ('Pedro Correia', 'pcorreia25@gmail.com'),
    ('Italo Mandara', 'italomandara@gmail.com'),
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

# SHELL_PLUS = "ipython"

DJANGO_DEBUG_TOOLBAR = True

# Application definition

BASE_INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.gis',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'django.contrib.redirects',
    'django.contrib.postgres',
    'djangosecure',
    'lib.audit',
    'lib.cache',
    'lib.common',
    'lib.viewslib',
    'accounts',
    'statici18n',
    'compressor'
]

INSTALLED_APPS = [
    'profiles',
    'company',
    'job_application',
    'curriculums',
    'addresses'

] + BASE_INSTALLED_APPS

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

ROOT_URLCONF = 'saturn.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(SITE_ROOT, 'templates'),
            os.path.join(SITE_ROOT, 'static'),
        ],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.csrf',

            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.eggs.Loader',
            ],
            'debug': DEBUG,
        },
    },
]

STATICFILES_DIRS = [
    os.path.join(SITE_ROOT, 'static', 'css'),
    os.path.join(SITE_ROOT, 'static', 'downloads'),
    os.path.join(SITE_ROOT, 'static', 'fonts'),
    os.path.join(SITE_ROOT, 'static', 'images'),
    os.path.join(SITE_ROOT, 'static', 'js'),
]
# STATICFILES_STORAGE = 'lib.storage.CachedStaticFilesStorage'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

# Statici18n Config
STATICI18N_ROOT = os.path.join(SITE_ROOT, 'static')
STATICFILES_DIRS += [STATICI18N_ROOT]

COMPRESS_ROOT = '/tmp/lz-static'

WSGI_APPLICATION = 'saturn.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True
TESTING = False

# User model
AUTH_USER_MODEL = 'accounts.User'
SOCIAL_AUTH_USER_MODEL = 'accounts.User'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    # 'accounts.registration_backends.Facebook22OAuth2'
)

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
)

# Pull slug max_length out ot
SLUG_MAX_LENGTH = 64

# Country Informations
COUNTRY_INFO_URL = "http://download.geonames.org/export/dump/countryInfo.txt"

BUGSNAG = { # YET TO COLLECT THE DATA
    "api_key": "",
    "js_api_key": "",
    "project_root": SITE_ROOT,
    "notify_release_stages": ["production", "staging", "preview", "development"],
    "ignore_classes": ["django.http.response.Http404"]
}
