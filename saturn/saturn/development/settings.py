from saturn.settings import *

# To see outgoing email dumped to a terminal, uncomment the following and
# run "python -m smtpd -n -c DebuggingServer localhost:1025"

DJANGOENV = 'development'

MIDDLEWARE_CLASSES += [
    'debug_toolbar.middleware.DebugToolbarMiddleware'
]

DEBUG = True

INSTALLED_APPS += [
    'django_nose',
    'django_extensions',
    'template_repl',
    'debug_toolbar'
]