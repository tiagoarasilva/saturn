from django.http import HttpResponseForbidden, HttpResponseRedirect
from lib.common.utils import reverse

class StaffOnlyMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return HttpResponseForbidden()
        return super(StaffOnlyMixin, self).dispatch(request, *args, **kwargs)


class UserAuthMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            # return HttpResponseForbidden()
            return HttpResponseRedirect(reverse('login'))
        return super(UserAuthMixin, self).dispatch(request, *args, **kwargs)

