{% if ENVIRONMENT == 'development' -%}
    {% set subdomain = 'saturn.' -%}
{% elif ENVIRONMENT == 'preview' -%}
    {% set subdomain = 'preview.' -%}
{% elif ENVIRONMENT == 'staging' -%}
    {% set subdomain = 'staging.' -%}
{% elif ENVIRONMENT == 'live' -%}
    {% set subdomain = '' -%}
{% endif -%}
server {
    server_name {{ subdomain }}linezap.com;

    error_log /dev/stderr;
    access_log /dev/stdout;

    location /crossdomain.xml {
        return 200;
    }

    location /healthcheck/ {
        include uwsgi_params;
        uwsgi_param HTTP_HOST www.{{ subdomain }}linezap.com;
        uwsgi_param HTTP_X_FORWARDED_PROTO 'https';
        uwsgi_pass unix:/var/www/uwsgi.sock;
    }

    location / {
        return 301 https://www.{{ subdomain }}linezap.com$request_uri;
    }
}

server {
    server_name linezap.co.uk www.linezap.co.uk linezap.co www.linezap.co;
    access_log /dev/stdout;
    error_log /dev/stderr;

    location / {
       return 301 https://www.{{ subdomain }}linezap.com$request_uri;
    }
}


server {
    server_name branding.linezap.com branding.vienna.linezap.com;
    access_log /dev/stdout;
    error_log /dev/stderr;

    location / {
        satisfy any;
        deny all;
        auth_basic "Password Protected Access";
        auth_basic_user_file /var/www/deploy/htpasswd;
        root /var/www/branding;
    }
}


server {
    server_name www.{{ subdomain }}linezap.com ;

    error_log /dev/stderr;
    access_log /dev/stdout;

    client_max_body_size 100M;

    {% if ENVIRONMENT != 'development' -%}
    real_ip_header X-Forwarded-For;
    set_real_ip_from 0.0.0.0/0;
    {% else -%}
    add_header Host $host;
    add_header X-Forwarded-For $http_x_forwarded_for;
    add_header X-Forwarded-Proto $scheme;
    add_header X-CSS-Protection "1; mode=block";
    {% endif -%}

    sendfile off;

    location /favicon.ico {
        alias /var/www/saturn/static/favicon.ico;
    }

    location /touch.png {
        alias /var/www/saturn/static/touch.png;
    }

    location /touch-ipad.png {
        alias /var/www/saturn/static/touch-ipad.png;
    }

    location /touch-retina.png {
        alias /var/www/saturn/static/touch-retina.png;
    }

    location /robots.txt {
        alias /var/www/saturn/saturn/live/robots.txt;
    }

    location /crossdomain.xml {
        alias /var/www/saturn/saturn/live/crossdomain.xml;
    }

    location /google72bb21bffc4129aa.html {
        alias /var/www/saturn/saturn/live/google72bb21bffc4129aa.html;
    }

    location /pinterest-da1d1.html {
        alias /var/www/saturn/saturn/live/pinterest-da1d1.html;
    }

    location /pinterest-4d4e7.html {
        alias /var/www/saturn/saturn/live/pinterest-4d4e7.html;
    }

    location /channel.html {
        alias /var/www/saturn/saturn/live/fbchannel.html;
    }

    location /error.css {
        alias /var/www/saturn/saturn/live/error.css;
    }

    location /error.html {
        alias /var/www/saturn/templates/foundation5/v1/500.html;
    }

    location /500.svg {
        alias /var/www/saturn/saturn/live/500.svg;
    }

    location /logo-white.svg {
       alias /var/www/saturn/saturn/live/logo-white.svg;
    }

    location /fonts {
      alias /var/www/saturn/static/fonts;
    }

    {% if ENVIRONMENT != 'development' -%}
    error_page 500 501 502 503 504 =200 /error.html;
    {% endif -%}

    location /art-of-the-day/index.atom              { return 410; }
    location /works/collected/index.atom             { return 410; }
    location ~ /user/(.*)/collection/(.*)/index.atom { return 410; }
    location ~ /user/(.*)/works/collected.atom       { return 410; }
    location ~ /apple-touch-icon(.*)?.png            { rewrite ^(.*)$ https://d2m7ibezl7l5lt.cloudfront.net/static/img/touch.2f332a25a40e.png; }

    {% if ENVIRONMENT == 'development' -%}
    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_read_timeout 300s;
        proxy_set_header Host $host;
    }

    {% else -%}
    location /framing/add-to-basket/ {
        include uwsgi_params;
        uwsgi_pass unix:/var/www/uwsgi.sock;
        uwsgi_read_timeout 300s;
    }

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/var/www/uwsgi.sock;
        uwsgi_read_timeout 300s;

        {% if ENVIRONMENT != 'live' -%}
        satisfy any;
        deny all;
        auth_basic "Password Protected Access";
        auth_basic_user_file /var/www/deploy/htpasswd;
        {% endif -%}
    }
    {% endif -%}
}
